//
//  MapPresenter.swift
//  Weather
//
//  Created by Oleg Babura on 06.08.2021.
//

import Foundation
import MapKit

protocol MapViewProtocol: class {
    func dismiss()
}

protocol MapPresenterDelegate: class {
    func didSelectLocation(coorginates: CLLocationCoordinate2D)
}

protocol MapPresenterProtocol: class {
    init(view: MapViewProtocol, router: MapRouterProtocol, delegate: MapPresenterDelegate?)
    
    func didChooseLocation(coordinate: CLLocationCoordinate2D)
}

class MapPresenter: MapPresenterProtocol {
    required init(view: MapViewProtocol, router: MapRouterProtocol, delegate: MapPresenterDelegate? = nil) {
        self.view = view
        self.router = router
        self.delegate = delegate
    }
    
    weak var view: MapViewProtocol?
    weak var delegate: MapPresenterDelegate?
    var router: MapRouterProtocol!
    
    func didChooseLocation(coordinate: CLLocationCoordinate2D) {
        delegate?.didSelectLocation(coorginates: coordinate)
        router?.popViewControlelr()
    }
}
