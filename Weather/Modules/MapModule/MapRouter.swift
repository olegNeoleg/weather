//
//  MapRoutet.swift
//  Weather
//
//  Created by Oleg Babura on 10.08.2021.
//

import UIKit

protocol MapRouterProtocol: RouterProtocol {}

class MapRouter: MapRouterProtocol {
    required init(navigationController: UINavigationController, assembly: AssemblyProtocol) {
        self.navigationController = navigationController
        self.assembly = assembly
    }
    var navigationController: UINavigationController!
    var assembly: AssemblyProtocol!
}
