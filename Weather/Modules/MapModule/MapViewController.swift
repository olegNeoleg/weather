//
//  MapViewController.swift
//  Weather
//
//  Created by Oleg Babura on 06.08.2021.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet private weak var mapView: MKMapView!
    var presenter: MapPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let longPressGesstureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(sender:)))
        mapView.addGestureRecognizer(longPressGesstureRecognizer)
    }
    

    @objc func handleLongPress(sender: UILongPressGestureRecognizer) {
        if sender.state != .began { return }
        let touchLocation = sender.location(in: mapView)
        let locationCoordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)
        
        presenter.didChooseLocation(coordinate: locationCoordinate)
    }

}

extension MapViewController: MapViewProtocol {
    func dismiss() {
        navigationController?.popViewController(animated: true)
    }
}
