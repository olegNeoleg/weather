//
//  SearchPresenter.swift
//  Weather
//
//  Created by Oleg Babura on 06.08.2021.
//

import Foundation
import MapKit

protocol SearchViewProtocol: class {
    func setSearchResults(results: [String])
}

protocol SearchPresenterDelegate: class {
    func didSelectLocation(location: CLLocation)
}

protocol SearchPresenterProtocol: class {
    init(view: SearchViewProtocol, router: SearchRouterProtocol, delegate: SearchPresenterDelegate?)
    
    func registerForSearchUpdates()
    func didChangeSearchText(text: String)
    func didSelectSearchResult(title: String)
}

class SearchPresenter: NSObject, SearchPresenterProtocol {
    required init(view: SearchViewProtocol, router: SearchRouterProtocol, delegate: SearchPresenterDelegate? = nil) {
        self.view = view
        self.router = router
        self.delegate = delegate
    }
    
    weak var view: SearchViewProtocol?
    weak var delegate: SearchPresenterDelegate?
    var router: SearchRouterProtocol!
    
    private let searchCompleter = MKLocalSearchCompleter()
    
    func registerForSearchUpdates() {
        searchCompleter.delegate = self
        searchCompleter.resultTypes = .address
    }
    
    func didChangeSearchText(text: String) {
        text.count > 2 ? searchCompleter.queryFragment = text : view?.setSearchResults(results: [])
    }
    
    func didSelectSearchResult(title: String) {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(title) { [weak self] placemarks, error in
            guard let placemarks = placemarks, let location = placemarks.first?.location else {
                //handle no location found
                self?.router?.popViewControlelr()
                return
            }
            self?.delegate?.didSelectLocation(location: location)
            self?.router?.popViewControlelr()
        }
    }
}

extension SearchPresenter: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        var searchResult = Array<String>()
        for result in completer.results {
            searchResult.append(result.title)
        }
        view?.setSearchResults(results: searchResult)
    }
}
