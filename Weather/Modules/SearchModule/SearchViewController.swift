//
//  SearchViewController.swift
//  Weather
//
//  Created by Oleg Babura on 06.08.2021.
//

import UIKit
import MapKit

class SearchViewController: UIViewController {
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView!
    
    var presenter: SearchPresenterProtocol!
    var searhResults = Array<String>() {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.titleView = searchBar
        
        searchBar.tintColor = .white
        searchBar.searchTextField.textColor = .white
        searchBar.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        presenter.registerForSearchUpdates()
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searhResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = searhResults[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectSearchResult(title: searhResults[indexPath.row])
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.didChangeSearchText(text: searchText)
    }
}

extension SearchViewController: SearchViewProtocol {
    func setSearchResults(results: [String]) {
        self.searhResults = results
    }
}
