//
//  SearchRouter.swift
//  Weather
//
//  Created by Oleg Babura on 10.08.2021.
//

import UIKit

protocol SearchRouterProtocol: RouterProtocol {}

class SearchRouter: SearchRouterProtocol {
    required init(navigationController: UINavigationController, assembly: AssemblyProtocol) {
        self.navigationController = navigationController
        self.assembly = assembly
    }
    var navigationController: UINavigationController!
    var assembly: AssemblyProtocol!
}
