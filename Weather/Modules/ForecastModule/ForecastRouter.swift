//
//  ForecastRouter.swift
//  Weather
//
//  Created by Oleg Babura on 10.08.2021.
//

import UIKit

protocol ForecastRouterProtocol: RouterProtocol {
    func presentMapViewController(delegate: MapPresenterDelegate?)
    func presentSearchViewController(delegate: SearchPresenterDelegate?)
}

class ForecastRouter: ForecastRouterProtocol {
    required init(navigationController: UINavigationController, assembly: AssemblyProtocol) {
        self.navigationController = navigationController
        self.assembly = assembly
    }
    
    var assembly: AssemblyProtocol!
    var navigationController: UINavigationController!

    func presentMapViewController(delegate: MapPresenterDelegate?) {
        let viewController = assembly.buildMapModule(navigationController: navigationController, delegate: delegate)
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func presentSearchViewController(delegate: SearchPresenterDelegate?) {
        let viewController = assembly.buildSearchModule(navigationController: navigationController, delegate: delegate)
        navigationController.pushViewController(viewController, animated: true)
    }
}
