//
//  ViewController.swift
//  Weather
//
//  Created by Oleg Babura on 05.08.2021.
//

import UIKit

class ForecastViewController: UIViewController {
    
    private enum TableSections: Int, CaseIterable {
        case today, hourly, daily
    }
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var locationName: String! {
        didSet {
            title = locationName
        }
    }
    
    var presenter: ForecastPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.barTintColor = UIColor(hexString: "#4a90e2")
        navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ic_back")
        navigationController?.navigationBar.tintColor = .white
        navigationItem.backButtonTitle = ""
        
        let mapButtonItem = UIBarButtonItem(image: UIImage(named: "ic_my_location"), style: .plain, target: self, action: #selector(mapDidTap))
        mapButtonItem.tintColor = .white
        navigationItem.rightBarButtonItem = mapButtonItem
        
        tableView.allowsSelection = false
        
        tableView.register(TodayForecastCell.self)
        tableView.register(HourlyForecastCell.self)
        tableView.register(DailyForecastCell.self)
        
        presenter.updateLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchDidTap)))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.navigationBar.gestureRecognizers?.removeAll()
    }
    
    @objc func searchDidTap() {
        presenter.showSearch()
    }
    
    @objc func mapDidTap() {
        presenter.showMap()
    }
}

extension ForecastViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableSections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let forecast = presenter.forecast else { return 0 }
        switch section {
        case TableSections.today.rawValue:
            return 1
        case TableSections.hourly.rawValue:
            return 1
        case TableSections.daily.rawValue:
            return forecast.daily.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let forecast = presenter.forecast else { return UITableViewCell() }
        switch indexPath.section {
        case TableSections.today.rawValue:
            let cell = tableView.dequeue(TodayForecastCell.self, for: indexPath)
            cell.configure(locationName: locationName, model: forecast.today)
            return cell
        case TableSections.hourly.rawValue:
            let cell = tableView.dequeue(HourlyForecastCell.self, for: indexPath)
            cell.configure(forecast: forecast.hourly)
            return cell
        case TableSections.daily.rawValue:
            let cell = tableView.dequeue(DailyForecastCell.self, for: indexPath)
            cell.configure(model: forecast.daily[indexPath.row])
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension ForecastViewController: ForecastViewProtocol {
    func setLocationName(name: String) {
        locationName = name
    }
    
    func reloadData() {
        tableView.reloadData()
    }
}
