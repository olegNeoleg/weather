//
//  HourlyForecastCell.swift
//  Weather
//
//  Created by Oleg Babura on 06.08.2021.
//

import UIKit

class HourlyForecastCell: UITableViewCell {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    private var forecast = Array<ForecastItem>()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.register(HourlyForecastItemCell.self)
    }

    func configure(forecast: [ForecastItem]) {
        self.forecast = forecast
        collectionView.reloadData()
    }
}

extension HourlyForecastCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(HourlyForecastItemCell.self, forIndexPath: indexPath)
        cell.configure(model: forecast[indexPath.row])
        return cell
    }
}
