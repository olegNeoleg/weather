//
//  DailyForecastCell.swift
//  Weather
//
//  Created by Oleg Babura on 06.08.2021.
//

import UIKit

class DailyForecastCell: UITableViewCell {

    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var iconView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(model: ForecastItem) {
        let formatter = DateFormatter()
        formatter.dateFormat = "eeeeee"
        
        dateLabel.text = formatter.string(from: model.date)
        temperatureLabel.text = "\(model.minTemperature)°/\(model.maxTemperature)°"

        iconView.tintColor = .black
        iconView.image = model.weatherIcon(provider: IconProvider())
    }
}
