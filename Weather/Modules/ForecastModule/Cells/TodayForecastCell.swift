//
//  TodayForecastCell.swift
//  Weather
//
//  Created by Oleg Babura on 06.08.2021.
//

import UIKit

class TodayForecastCell: UITableViewCell {

    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var locationNameLabel: UILabel!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var humidityLabel: UILabel!
    @IBOutlet private weak var windSpeedLabel: UILabel!
    
    @IBOutlet private weak var weatherIconView: UIImageView!
    @IBOutlet private weak var windDirectionIconView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(locationName: String, model: ForecastItem) {
        let formatter = DateFormatter()
        formatter.dateFormat = "eeeeee, MMM d"
        
        dateLabel.text = formatter.string(from: model.date)
        let provider = IconProvider()
        weatherIconView.image = model.weatherIcon(provider: provider)
        windDirectionIconView.image = model.windDirectionIcon(provider: provider)
        
        locationNameLabel.text = locationName
        temperatureLabel.text = "\(model.minTemperature)°/\(model.maxTemperature)°"
        humidityLabel.text = "\(model.humidity)%"
        windSpeedLabel.text = "\(model.windSpeed)m/sec"
    }
    
}
