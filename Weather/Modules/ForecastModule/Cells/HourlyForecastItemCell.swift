//
//  HourlyForecastItemCell.swift
//  Weather
//
//  Created by Oleg Babura on 06.08.2021.
//

import UIKit

class HourlyForecastItemCell: UICollectionViewCell {
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var iconView: UIImageView!
    @IBOutlet private weak var temperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(model: ForecastItem) {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:MM"
        
        iconView.image = model.weatherIcon(provider: IconProvider())
        dateLabel.text = formatter.string(from: model.date)
        temperatureLabel.text = "\(model.maxTemperature)°"
    }
}
