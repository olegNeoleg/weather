//
//  ForecastPresenter.swift
//  Weather
//
//  Created by Oleg Babura on 05.08.2021.
//

import Foundation
import CoreLocation

protocol ForecastViewProtocol: class {
    func reloadData()
    func setLocationName(name: String)
}

protocol ForecastPresenterProtocol: class {
    init(view: ForecastViewProtocol, router: ForecastRouterProtocol, networkManager: NetworkManager)
    
    var forecast: Forecast? { get }
    
    func updateForecast(location: CLLocation)
    func updateLocation()
    func showMap()
    func showSearch()
}

class ForecastPresenter: NSObject, ForecastPresenterProtocol {
    weak var view: ForecastViewProtocol?
    var router: ForecastRouterProtocol!
    
    var forecast: Forecast? {
        didSet {
            view?.reloadData()
        }
    }
    
    let networkManager: NetworkManager!
    let locationManager = CLLocationManager()
    
    required init(view: ForecastViewProtocol, router: ForecastRouterProtocol, networkManager: NetworkManager) {
        self.view = view
        self.router = router
        self.networkManager = networkManager
    }
    
    func updateForecast(location: CLLocation) {
        let coder = CLGeocoder()
        coder.reverseGeocodeLocation(location) { [weak self] placemarks, error in
            if let error = error {
                debugPrint(error)
                return
            }
            var name = "Uknown Location"
            if let placemark = placemarks?.first {
                if let city = placemark.locality {
                    name = city
                } else if let country = placemark.country {
                    name = country
                } else if let ocean = placemark.ocean {
                    name = ocean
                }
            }
            
            self?.networkManager.loadForecast(lat: location.coordinate.latitude, lon: location.coordinate.longitude) { [weak self] result in
                switch result {
                case.success(let forecast):
                    self?.forecast = forecast
                    self?.view?.setLocationName(name: name)
                case .failure(let error):
                    debugPrint(error)
                }
            }
        }
    }
    
    func updateLocation() {
        locationManager.delegate = self
        
        if locationManager.authorizationStatus != .authorizedWhenInUse {
            locationManager.requestWhenInUseAuthorization()
        } else {
            locationManager.requestLocation()
        }
    }
    
    func showMap() {
        router?.presentMapViewController(delegate: self)
    }
    
    func showSearch() {
        router?.presentSearchViewController(delegate: self)
    }
}

extension ForecastPresenter: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if manager.authorizationStatus == .authorizedWhenInUse {
            manager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        updateForecast(location: location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        debugPrint(error)
    }
}

extension ForecastPresenter: MapPresenterDelegate {
    func didSelectLocation(coorginates: CLLocationCoordinate2D) {
        let location = CLLocation(latitude: coorginates.latitude, longitude: coorginates.longitude)
        updateForecast(location: location)
    }
}

extension ForecastPresenter: SearchPresenterDelegate {
    func didSelectLocation(location: CLLocation) {
        updateForecast(location: location)
    }
}
