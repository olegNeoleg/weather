//
//  Forecast.swift
//  Weather
//
//  Created by Oleg Babura on 10.08.2021.
//

import Foundation

class Forecast {
    var today: ForecastItem!
    var hourly: [ForecastItem]!
    var daily: [ForecastItem]!
}

struct ForecastPeriodsResponce: Codable {
    var periods: [ForecastItem]
}

struct ForecastResponce: Codable {
    var success: Bool
    var error: Bool?
    var response: [ForecastPeriodsResponce]
}
