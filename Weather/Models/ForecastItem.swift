//
//  Forecast.swift
//  Weather
//
//  Created by Oleg Babura on 05.08.2021.
//

import UIKit

struct ForecastItem: Codable {
    var date: Date
    var minTemperature: Int
    var maxTemperature: Int
    var humidity: Int
    var windSpeed: Int
    var windDirection: String
    var description: String
    
    enum CodingKeys: String, CodingKey {
        case date = "dateTimeISO"
        case minTemperature = "minTempC"
        case maxTemperature = "maxTempC"
        case humidity = "maxHumidity"
        case windSpeed = "windSpeedMaxKPH"
        case windDirection = "windDir"
        case description = "weatherPrimaryCoded"
    }
    
    func weatherIcon(provider: IconProviderProtocol) -> UIImage {
        return provider.weatherIcon(description: self.description, date: self.date)
    }
    
    func windDirectionIcon(provider: IconProviderProtocol) -> UIImage {
        return provider.windDirectionIcon(direction: self.windDirection)
    }
}

extension ForecastItem {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        minTemperature = try container.decode(Int.self, forKey: .minTemperature)
        maxTemperature = try container.decode(Int.self, forKey: .maxTemperature)
        humidity = try container.decode(Int.self, forKey: .humidity)
        windSpeed = try container.decode(Int.self, forKey: .windSpeed)
        windDirection = try container.decode(String.self, forKey: .windDirection)
        description = try container.decode(String.self, forKey: .description)
        
        let dateString = try container.decode(String.self, forKey: .date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = dateFormatter.date(from: dateString) else {
            throw DecodingError.dataCorruptedError(forKey: .date, in: container, debugDescription: "Date string does not match format expected by formatter")
        }
        
        self.date = date
    }
}
