//
//  UrlBuilder.swift
//  Weather
//
//  Created by Oleg Babura on 10.08.2021.
//

import Foundation

protocol UrlBuilderProtocol: class {
    func build(for lat: Double, lon: Double, granularity: ForecastGranularity) -> URL?
}

class ForecastUrlBuilder: UrlBuilderProtocol {
    private enum Keys {
        static let secretKey = "QDks25mca8khEqSOYQUGiUSnSBXVXmaUW4pmXU0H"
        static let accessId = "ByBKeK4nG7WPBOV5tSG89"
    }
    
    private enum Paths {
        static let baseUrl: String = "https://api.aerisapi.com/forecasts/"
        static let responceFormat: String = "?format=json"
        static let fields: String = "fields=periods.dateTimeISO,periods.maxTempC,periods.minTempC,periods.maxHumidity,periods.windSpeedMaxKPH,periods.windDir,periods.weatherPrimaryCoded"
        static let filter = "filter="
        static let limit = "limit="
        static let clientId = "client_id="
        static let clientSecret = "client_secret="
        static let binder = "&"
    }
    
    private struct RequestConfiguration {
        private(set) var limit: String
        private(set) var filter: String
        
        init(granularity: ForecastGranularity) {
            switch granularity {
            case .today:
                limit = "\(1)"
                filter = "day"
            case .hourly:
                limit = "\(8)"
                filter = "3hr"
            case .daily:
                limit = "\(16)"
                filter = "day"
            }
        }
    }
    
    func build(for lat: Double, lon: Double, granularity: ForecastGranularity) -> URL? {
        let configuration = RequestConfiguration(granularity: granularity)
        let urlString = Paths.baseUrl + "\(lat),\(lon)" + Paths.responceFormat + Paths.binder + Paths.filter + configuration.filter + Paths.binder + Paths.limit + configuration.limit + Paths.binder + Paths.fields + Paths.binder + Paths.clientId + Keys.accessId + Paths.binder + Paths.clientSecret + Keys.secretKey
        return URL(string: urlString)
    }
}
