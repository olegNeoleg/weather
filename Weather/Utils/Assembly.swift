//
//  ForecastRouter.swift
//  Weather
//
//  Created by Oleg Babura on 10.08.2021.
//

import UIKit

protocol AssemblyProtocol: class {
    func buildInitialModule() -> UIViewController
    func buildSearchModule(navigationController: UINavigationController, delegate: SearchPresenterDelegate?) -> UIViewController
    func buildMapModule(navigationController: UINavigationController, delegate: MapPresenterDelegate?) -> UIViewController
}

class Assembly: AssemblyProtocol {
    func buildInitialModule() -> UIViewController {
        let viewController = UIStoryboard.main.instantiate(ForecastViewController.self)
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let urlBuilder = ForecastUrlBuilder()
        let networkManager = ForecastNetworkManager(urlBuilder: urlBuilder)
        let router = ForecastRouter(navigationController: navigationController, assembly: self)
        let presenter = ForecastPresenter(view: viewController, router: router, networkManager: networkManager)
        
        viewController.presenter = presenter
        
        return navigationController
    }
    
    func buildSearchModule(navigationController: UINavigationController, delegate: SearchPresenterDelegate? = nil) -> UIViewController {
        let viewController = UIStoryboard.main.instantiate(SearchViewController.self)
        let router = SearchRouter(navigationController: navigationController, assembly: self)
        let presenter = SearchPresenter(view: viewController, router: router, delegate: delegate)
        viewController.presenter = presenter
        
        return viewController
    }
    
    func buildMapModule(navigationController: UINavigationController, delegate: MapPresenterDelegate? = nil) -> UIViewController {
        let viewController = UIStoryboard.main.instantiate(MapViewController.self)
        let router = MapRouter(navigationController: navigationController, assembly: self)
        let presenter = MapPresenter(view: viewController, router: router, delegate: delegate)
        viewController.presenter = presenter
        
        return viewController
    }
}
