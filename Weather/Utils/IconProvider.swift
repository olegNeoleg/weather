//
//  IconProvider.swift
//  Weather
//
//  Created by Oleg Babura on 10.08.2021.
//

import UIKit

protocol IconProviderProtocol {
    func weatherIcon(description: String, date: Date) -> UIImage
    func windDirectionIcon(direction: String) -> UIImage
}

class IconProvider: IconProviderProtocol {
    private enum WeatherCode: String {
        case CL = "CL"
        case FW = "FW"
        case SC = "SC"
        case BK = "BK"
        case OV = "OV"
        case A = "A"
        case BD = "BD"
        case BN = "BN"
        case BR = "BR"
        case BS = "BS"
        case BY = "BY"
        case F = "F"
        case FR = "FR"
        case H = "H"
        case IC = "IC"
        case IF = "IF"
        case IP = "IP"
        case K = "K"
        case L = "L"
        case R = "R"
        case RW = "RW"
        case RS = "RS"
        case SI = "SI"
        case WM = "WM"
        case S = "S"
        case SW = "SW"
        case T = "T"
        case UP = "UP"
        case VA = "VA"
        case WP = "WP"
        case ZF = "ZF"
        case ZL = "ZL"
        case ZR = "ZR"
        case ZY = "ZY"
    }
    
    private enum WindDirectionCode: String {
        case N = "N"
        case NNE = "NNE"
        case NE = "NE"
        case ENE = "ENE"
        case E = "E"
        case ESE = "ESE"
        case SE = "SE"
        case SSE = "SSE"
        case S = "S"
        case SSW = "SSW"
        case SW = "SW"
        case WSW = "WSW"
        case W = "W"
        case WNW = "WNW"
        case NW = "NW"
        case NNW = "NNW"
    }
    
    func weatherIcon(description: String, date: Date) -> UIImage {
        let weatherStringCodes = description.components(separatedBy: ":")
        
        if let weatherStringCode = weatherStringCodes.last, let weatherCode = WeatherCode(rawValue: weatherStringCode) {
            
            let dayTime = date.isDayTime() ? "day" : "night"
            var condition: String
            
            switch weatherCode {
            case .R:
                condition = "rain"
            case .RW:
                condition = "shower"
            case .CL, .FW:
                condition = "bright"
            case .BK, .SC, .OV:
                condition = "cloudy"
            case .T:
                condition = "thunder"
            default:
                condition = "clouds"
            }
            
            let imageName = "ic_white_\(dayTime)_\(condition)"
            return UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate) ?? UIImage()
        }
        return UIImage()
    }
    
    func windDirectionIcon(direction: String) -> UIImage {
        if let windCode = WindDirectionCode(rawValue: direction) {
            var direction: String
            switch windCode {
            case .N:
                direction = "n"
            case .E:
                direction = "e"
            case .S:
                direction = "s"
            case .W:
                direction = "w"
            case .NE, .NNE:
                direction = "ne"
            case .SE, .SSE:
                direction = "se"
            case .SW, .SSW:
                direction = "ws"
            case .NW, .NNW:
                direction = "wn"
            default:
                direction = "n"
            }
            
            let imageName = "icon_wind_\(direction)"
            return UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate) ?? UIImage()
        }
        return UIImage()
    }
}

fileprivate extension Date {
    func isDayTime() -> Bool {
        let hour = Calendar.current.component(.hour, from: self)

        switch hour {
        case 6..<20:
            return true
        default:
            return false
        }
    }
}
