//
//  Router.swift
//  Weather
//
//  Created by Oleg Babura on 06.08.2021.
//

import UIKit

protocol RouterProtocol: class {
    init(navigationController: UINavigationController, assembly: AssemblyProtocol)
    
    var navigationController: UINavigationController! { get }
    var assembly: AssemblyProtocol! { get }
    
    func popViewControlelr ()
}

extension RouterProtocol {
    func popViewControlelr () {
        navigationController.popViewController(animated: true)
    }
}
