//
//  NetworkManager.swift
//  Weather
//
//  Created by Oleg Babura on 05.08.2021.
//

import Foundation
import Alamofire

enum ForecastGranularity: CaseIterable {
    case today, hourly, daily
}

protocol NetworkManager: class {
    init(urlBuilder: UrlBuilderProtocol)
    
    typealias ForecastItemsCompletion = ((Result<[ForecastItem], Error>))->()
    typealias ForecastCompletion = ((Result<Forecast, Error>))->()
    
    func loadForecastItem(lat: Double, lon: Double, granularity: ForecastGranularity, completion: @escaping ForecastItemsCompletion)
    func loadForecast(lat: Double, lon: Double, completion: @escaping ForecastCompletion)
}

class ForecastNetworkManager: NetworkManager {
    enum NetworkError: String, Error {
        case urlError = "Wrong URL format"
        case loadingError = "Loading data error"
        case parseError = "Parsing data error"
    }
    
    required init(urlBuilder: UrlBuilderProtocol) {
        self.urlBuilder = urlBuilder
    }
    
    private var urlBuilder: UrlBuilderProtocol
    
    func loadForecastItem(lat: Double, lon: Double, granularity: ForecastGranularity, completion: @escaping ForecastItemsCompletion) {
        
        guard let url = urlBuilder.build(for: lat, lon: lon, granularity: granularity) else {
            completion(.failure(NetworkError.urlError))
            return
        }
        
        AF.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(_):
                do {
                    let forecastMetadata = try JSONDecoder().decode(ForecastResponce.self, from: response.data!)
                    guard let forecast = forecastMetadata.response.first else {
                        completion(.failure(NetworkError.parseError))
                        return
                    }
                    completion(.success(forecast.periods))
                } catch let error {
                    completion(.failure(error))
                    return
                }
            case .failure(let error):
                completion(.failure(error))
                return
            }
        }
    }
    
    func loadForecast(lat: Double, lon: Double, completion: @escaping ForecastCompletion) {
        let group = DispatchGroup()
        let forecast = Forecast()
        var blockError: Error?
        for granularity in ForecastGranularity.allCases {
            group.enter()
            loadForecastItem(lat: lat, lon: lon, granularity: granularity) { result in
                defer {
                    group.leave()
                }
                
                switch result {
                case .success(let forecastItems):
                    switch granularity {
                    case .today:
                        guard let item = forecastItems.first else {
                            blockError = NetworkError.loadingError
                            return
                        }
                        forecast.today = item
                    case .hourly:
                        forecast.hourly = forecastItems
                    case .daily:
                        forecast.daily = forecastItems
                    }
                case .failure(let error):
                    blockError = error
                    return
                }
            }
        }
        group.notify(queue: .main) {
            if let error = blockError {
                completion(.failure(error))
                return
            }
            completion(.success(forecast))
        }
    }
}
