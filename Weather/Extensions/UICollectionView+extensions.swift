//
//  UICollectionView+extensions.swift
//  Weather
//
//  Created by Oleg Babura on 05.08.2021.
//

import UIKit

extension UICollectionView {
    public func register<T: UICollectionViewCell>(_ cellClass: T.Type) {
        register(UINib(nibName: String(describing: cellClass), bundle: nil), forCellWithReuseIdentifier: String(describing: cellClass))
    }

    public func dequeue<T: UICollectionViewCell>(_ cellClass: T.Type, forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: String(describing: cellClass), for: indexPath) as? T else {
            fatalError("Error: cell with id: \(String(describing: cellClass)) for indexPath: \(indexPath) is not \(T.self)")
        }
        return cell
    }
}
