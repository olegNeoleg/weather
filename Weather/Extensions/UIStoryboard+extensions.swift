//
//  UIStoryboard+extensions.swift
//  Weather
//
//  Created by Oleg Babura on 05.08.2021.
//

import UIKit

extension UIStoryboard {
    class var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle:nil)
    }
    
    public func instantiate<T: UIViewController>(_ viewControllerClass: T.Type) -> T {
        guard let vc = self.instantiateViewController(withIdentifier: String(describing: viewControllerClass.self)) as? T else {
            fatalError("Could not instantiate view controller \(T.self)") }
        return vc
    }
}
