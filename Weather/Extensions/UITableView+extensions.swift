//
//  UITableView+extensions.swift
//  Weather
//
//  Created by Oleg Babura on 05.08.2021.
//

import UIKit

extension UITableView {
    public func register<T: UITableViewCell>(_ cellClass: T.Type) {
        register(UINib(nibName: String(describing: cellClass), bundle: nil), forCellReuseIdentifier: String(describing: cellClass))
    }

    public func dequeue<T: UITableViewCell>(_ cellClass: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: cellClass), for: indexPath) as? T else {
            fatalError("Error: cell with id: \(String(describing: cellClass)) for indexPath: \(indexPath) is not \(T.self)")
        }
        return cell
    }
}
